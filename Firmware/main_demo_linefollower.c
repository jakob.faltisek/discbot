#include <avr/io.h>
#include <util/delay.h>
#include <uart_basic.h>
#include <timebase.h>
#include "motioncontrol.h"
#include "cmdDispatcher.h"
#include "ColorSensor.h"

void lineRecognition();
uint8_t ozoColorRecognition();


 //defines that specify the speed of the robot for each position 
 // of the line
 #define SPEED_NO_LINE				60
 #define SPEED_LINE_SIDE			55
 #define SPEED_LINE_MIDDLE			60
 #define SPEED_LINE_SIDE_AND_MIDDLE	60
 //definitions that specify the direction of the robot
 // (how much it turns left or right) for the outer positions of the line
 #define DIR_LINE_SIDE				80
 #define DIR_LINE_SIDE_AND_MIDDLE	30

 //global variable that indicates that a color measurement has finished
volatile uint8_t colorValReady;
 //global variable that stores the measured color values for each sensor
volatile int8_t colorVal[3];

//global variable that indicades that the linefollowing is enabled
volatile uint8_t lineFollowingEnabled;


void onColorValReady(uint8_t len, int8_t data[])
{
	for (uint8_t i = 0; i < len; i++)
		colorVal[i] = data[i];

	colorValReady = 1;
}


//Command:
//starts or stops the line following mode
// |           |  CODE |  LEN  |     DATA0     |  
// |:----------|:-----:|:-----:|:-------------:|
// | COMMAND   | 0x11  |   1   |0=stop, 1=start| 
// | RESPONSE  | 0x11  |   0   |
#define CD_CMD_LINEFOLLOWER					0x11

// this function is called by the command dispatcher once a command
// has been received by the UART2, on which the BLE module is connected
void cd_executeCommand(uint8_t uartNo, uint8_t cmd, uint8_t len)
{

	switch (cmd)
	{
		case CD_CMD_LINEFOLLOWER: 
			//Command form the line follower page of the app was received

			if (cd_getData(uartNo) == 1)	
			{
				lineFollowingEnabled = 1;		//Enable linefollowing
				colorValReady = 0;				//discard measurement if there is one stored
				cs_getColor(onColorValReady);	//start first measurement
			}
			else
			{
				lineFollowingEnabled = 0;		//Stop linefollowing
				mc_setSpeed(0);					//stop motor
			}

			// send a response to the UART2
			cd_respond(uartNo, CD_CMD_LINEFOLLOWER, 0);	
		break;

		default:
			cd_respond(uartNo, CD_CMD_UNKNOWN, 0); // in case an unknown command has been received
		break;                                     
	}

}

// this function determines the position of the line and sets the
// speed and direction of the motion-control lib accordingly
void lineRecognition()
{
	if (colorVal[CS_LEFT] != 0 && colorVal[CS_RIGHT] == 0 && colorVal[CS_MIDDLE] == 0)
	{	//line under the left sensor
		mc_setSpeed(SPEED_LINE_SIDE);
		mc_setDirection(DIR_LINE_SIDE);
	}
	else if (colorVal[CS_LEFT] != 0 && colorVal[CS_RIGHT] == 0)
	{	//line between the left and the middle sensor
		mc_setSpeed(SPEED_LINE_SIDE_AND_MIDDLE);
		mc_setDirection(DIR_LINE_SIDE_AND_MIDDLE);
	}
	else if (colorVal[CS_RIGHT] != 0 && colorVal[CS_LEFT] == 0 && colorVal[CS_MIDDLE] == 0)
	{	//line under the right sensor
		mc_setSpeed(SPEED_LINE_SIDE);
		mc_setDirection(-DIR_LINE_SIDE);
	}
	else if (colorVal[CS_RIGHT] != 0 && colorVal[CS_LEFT] == 0)
	{	//line between the right and the middle sensor
		mc_setSpeed(SPEED_LINE_SIDE_AND_MIDDLE);
		mc_setDirection(-DIR_LINE_SIDE_AND_MIDDLE);
	}
	else if (colorVal[CS_MIDDLE] != 0 && colorVal[CS_LEFT] == 0 && colorVal[CS_RIGHT] == 0)
	{	//line under the middle sensor
		mc_setSpeed(SPEED_LINE_MIDDLE);
		mc_setDirection(0);
	}
	else if (colorVal[CS_MIDDLE] == 0 && colorVal[CS_LEFT] == 0 && colorVal[CS_RIGHT] == 0)
	{	//no line under the sensors recognized 
		mc_setSpeed(SPEED_NO_LINE);
		mc_setDirection(0);
	}
}


int main(void)
{
	//initialize the timebase
	tb_init(TB_10MS, 10); 

	//initialize the command dispatcher
	cd_init();
	cd_configUART(CD_UART2, 9600, 100, 200, 10); // initialize uart2 (BLE module) for the command dispatcher

	//initialize the motor driver via motion-control library
	mc_init(0);

	//set speed to 0 to stop the discbot at the beginning
	mc_setSpeed(0); 
	
	//initialize the color-sensor with the address of the eeprom
	//	where the reference and color values are stored
	cs_init(100);
 

    while (1) 
    {
		if (lineFollowingEnabled)
		{
			if (colorValReady) //when measurement has finished
			{
				colorValReady = 0; //clear indication

				//determine the position of the line and set the
				//speed and direction accordingly
				lineRecognition();

				//start the next measurement
				cs_getColor(onColorValReady);
				}
			}

	}
		
}
