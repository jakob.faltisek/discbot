// ----------------------------------------------------------------------------
/// @file         ColorSensor.h
/// @addtogroup   COLOR_SENSOR_LIB    COLOR_SENSOR
/// @{
/// @brief        The ColorSensor library provides a set of functions to control the 
///				  ColorSensor Board of the DiscBot
/// @author       Jakob Faltisek
// ----------------------------------------------------------------------------


#ifndef COLORSENSOR_H_
#define COLORSENSOR_H_


// ----------------------------------------------------------------------------
/// @brief			  DDR of the S0 and S1 input of the color sensor
#define		DDR_S01		DDRA
/// @brief			  PORT of the S0 and S1 input of the color sensor
#define		PORT_S01	PORTA
/// @brief			  Pin-Number of the S0 input of the color sensor
#define		S0			PA0
/// @brief			  Pin-Number of the S1 input of the color sensor
#define		S1			PA1
/// @brief			  used to set the frequency scaling inputs of the color sensors
typedef enum CS_FreqScale {
	POWER_DOWN, 		///< sensor is turned off
	PERCENT20, 			///< frequency scale is at 20%
	PERCENT2, 			///< frequency scale is at 2%
	PERCENT100			///< frequency scale is at 100%
};
// ----------------------------------------------------------------------------



// ----------------------------------------------------------------------------
/// @brief			  DDR of the S2 and S3 input of the color sensor
#define		DDR_S23		DDRA
/// @brief			  PORT of the S2 and S3 input of the color sensor
#define		PORT_S23	PORTA
/// @brief			  Pin-Number of the S2 input of the color sensor
#define		S2			PA2
/// @brief			  Pin-Number of the S3 input of the color sensor
#define		S3			PA3
/// @brief			  used to set the photodiode type selection inputs inputs of the color sensors
typedef enum CS_ColorSel {
	RED, 				///< red photodiode is selected
	CLEAR, 				///< all photodiode are selected ("white")
	BLUE, 				///< blue photodiode is selected
	GREEN				///< green photodiode is selected
};
// ----------------------------------------------------------------------------


// ----------------------------------------------------------------------------
/// @brief			  DDR of the A and B input of the DEMUX
#define		DDR_AB		DDRA
/// @brief			  PORT of the A and B input of the DEMUX
#define		PORT_AB		PORTA
/// @brief			  Pin-Number of the A input of the DEMUX
#define		A			PA4
/// @brief			  Pin-Number of the B input of the DEMUX
#define		B			PA5
/// @brief			  used to set the inputs of the DEMUX to select a color sensor
typedef enum CS_SensorSel {
	CS_LEFT, 			///< the left color sensor is selected
	CS_MIDDLE, 			///< the middle color sensor is selected	
	CS_RIGHT, 			///< the right color sensor is selected
	NO_SENSOR			///< no colorsensor is selected
};
// ----------------------------------------------------------------------------


//Frequency output of the color sensors
// ----------------------------------------------------------------------------
/// @brief			  DDR of the frequency output of the color sensor
#define		DDR_OUT		DDRD
/// @brief			  PIN of the frequency output of the color sensor
#define		PIN_OUT		PIND
/// @brief			  Pin-Number of the frequency output of the color sensor (Counter Input T0)
#define		OUT			PD7 //T0
// ----------------------------------------------------------------------------

//Pin to turn the LEDs of the pcb on
//	L	=>	leds are off
//	H	=>	leds are on
// ----------------------------------------------------------------------------
/// @brief			  DDR of the output, that controls the white LED's 
#define		DDR_LEDS	DDRA
/// @brief			  PORT of the output, that controls the white LED's 
#define		PORT_LEDS	PORTA
/// @brief			  Pin-Number of the output, that controls the white LED's 
#define		LEDS		PA6
// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------
/// @brief			  used to store a rgb color
 struct RGB_Value {
	 uint8_t		r;			///< red value
	 uint8_t		g;			///< green value
	 uint8_t		b;			///< blue value
 };
// ----------------------------------------------------------------------------

 //dataLen, dass man die callbackfunction auch verwenden k�nnte, wenn es 4 Sensoren w�ren
 //		Interface bleibt das selbe
 
 // ----------------------------------------------------------------------------
/// @brief			  define the type of the callback function
 typedef void (*callback_func)(uint8_t dataLen, int8_t* pData);
// ----------------------------------------------------------------------------


 // ----------------------------------------------------------------------------
/// @brief		  initialize the in and outputs and the timers of the color sensor board 
/// @detail       initialize the in and outputs of the color sensor and
///				  configures the Timer0 (as counter) and the Timer4
///				  this function has to be called, in order to get all the other functions to work		
/// @param[in]    eePromAdr		the adress of the first eeprom byte, which is required
///								to store the color and the reference. 
///								(The following 19 + number of stored color * 3 addresses 
///								have to be available )
/// @retval		  0    no reference was stored in the eeprom
/// @ratval		  1    reference has been found in the eeprom
// ----------------------------------------------------------------------------
 void cs_init(uint16_t eePromAdr);

 // ----------------------------------------------------------------------------
/// @brief        take a reference and store the colors in the eeprom
/// @param[in]    nrColors		the number of color, that are measured
///								and stored in the eeprom
// ----------------------------------------------------------------------------
 void cs_config(uint8_t nrColors);

 // ----------------------------------------------------------------------------
/// @brief        take a reference and store it in the eeprom
/// @retval       1   color was set successfully
/// @retval       0   a problem happend, color not stored
// ----------------------------------------------------------------------------
 uint8_t cs_setReference();

 // ----------------------------------------------------------------------------
/// @brief        measure one color and store it in the eeprom
/// @param[in]    colorNr		the number of the color, that is to be stored
/// @retval       1   color was set successfully
/// @retval       0   a problem happend, color not stored
// ----------------------------------------------------------------------------
 uint8_t cs_setColor(uint8_t colorNr);

// ----------------------------------------------------------------------------
/// @brief        measere the color under each sensor and return the values
/// @param[in]    callbackFunc	address of the callback function
/// @retval		  1   measurement has been successfully started
/// @retval		  0   an measurement is already running
// ----------------------------------------------------------------------------
 uint8_t cs_getColor(callback_func callbackFunc);

#endif /* COLORSENSOR_H_ */

/// @}