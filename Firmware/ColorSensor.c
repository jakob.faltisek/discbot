/*
 * Sensorplatine.c
 *
 * Created: 10.08.2017 17:27:00
 *  Author: Jakob
 */ 

// #define	F_CPU	16000000
 #include <stdlib.h>
 #include <avr/io.h>
 #include <avr/interrupt.h>
 #include <util/delay.h>
 #include "uart_basic.h"
 #include "eeprom.h"
 #include "colorSensor.h"


 //#define _DEBUG_
 
 //global variable where the overflows of the TCNT0 are count
 static volatile uint16_t	cntOVF0;

 //global variable for the measurement
 static callback_func callbackFunction;
 static volatile uint8_t			measurementIsRunning;
 static volatile struct RGB_Value	tmpMeasureVal[3];

 //global variable for the eeprom address
 static volatile uint16_t	eepromAddress = 0;

 //definitions on how many average circles for the reference (cs_setReference() ) and
 //the color measurement (cs_setColor(colorNr) ) should be made 
 #define	NUM_REF_SAMPLES		10
 #define	NUM_COLOR_SAMPLES	5

 //global variable for the number of stored colors
 static uint8_t				nrStoredColors;
 //shows if a reference was stored in the eeprom
 static uint8_t				refStored;
 //global array for the stored reference 
 static uint16_t			storedReference[3][3];
 //pointer for the array, where the color values are stored
 //	the memory will be alloced in the init function 
 static struct RGB_Value*	storedColors;

 #define	MAX_OUTPUT_VALUE		7

 #define	THRESHOLD_DIFFERENCE	3

 //definitions for the relative eeprom addresses,
 //starting from the eePromAddress value
 #define	EEPROM_ADR_REF		1 //size: 3*3*2 = 18 byte
 #define	EEPROM_ADR_NR_COL	0 //size: 1 byte
 #define	EEPROM_ADR_COLORS	19 //size: 3*nrColors byte

 //prototypes of some functions
 void setOutFreqScaling(enum CS_FreqScale val);
 void turnOnLEDs();
 void turnOffLEDs();
 void readColorFormEeprom(uint8_t colorNr);
 uint8_t readReferenceFromEeprom();

 void cs_init(uint16_t eePromAdr)
 {
	//frequency scaling pins as output
	DDR_S01 |= ((1<<S0) | (1<<S1));

	//photodiode type selection pins as output
	DDR_S23 |= ((1<<S2) | (1<<S3));

	//color sensor select pins as output
	DDR_AB |= ((1<<A) | (1<<B));

	//led pin as output
	DDR_LEDS |= (1<<LEDS);

	//frequency pin as input
	DDR_OUT &= ~(1<<OUT);

	//configure Timer4 for a timing of 1ms
	TCCR4B = 0x00; //timer stopped
	TIMSK4 |= (1<<OCIE1A); //activate overflow interrupt on compare match
	OCR4A = 15999; //top value
	
	//configure timer0 for the counting of the Frequency impulses
	TCCR0B = 0x00; //timer stopped
	TIMSK0 |= (1<<TOIE0); //activate overflow interrupt

	//enable global interrupts
	sei();

	//set the output frequency scaling to the maximum
	setOutFreqScaling(PERCENT100);

	//turn on LEDs
	turnOnLEDs();

	//store the eeprom address in the global variable
	eepromAddress = eePromAdr;

	refStored = 1;
	
	//read the reference from the given eeprom address and store it in the global variable
	if (!readReferenceFromEeprom())
		refStored = 0;

	//read number of stored colors from the eeprom
	if ( (nrStoredColors = eeprom_read(eepromAddress+EEPROM_ADR_NR_COL) ) == 0xFF)
		refStored = 0;

	#ifdef _DEBUG_
		uart0_newLine();
		uart0_puts("num:");
		uart0_putCharAsDigits(nrStoredColors);
		uart0_puts("ref:");
		uart0_putCharAsDigits(refStored);
		uart0_newLine();
	#endif

	if (refStored)
	{

		//create dynamically the global array, where the colors are stored
		storedColors = malloc(sizeof(struct RGB_Value) * nrStoredColors);

		//set the Color 0 to the value of the reference
		storedColors[0].r = MAX_OUTPUT_VALUE;
		storedColors[0].g = MAX_OUTPUT_VALUE;
		storedColors[0].b = MAX_OUTPUT_VALUE;


		//read the stored color values from the eeprom and store them in the global array
 		for (uint8_t i = 1; i <= nrStoredColors; i++) {
	 		readColorFormEeprom(i);
 		}
	}

	//initialize the global variables
	measurementIsRunning = 0;
	callbackFunction = NULL;
	
 }

 void cs_config (uint8_t nrColors)
 {
	//free the storage in cs_init create array
	//free(storedColors);
	//create dynamically the global array, where the colors are stored 
	storedColors = malloc(sizeof(struct RGB_Value) * nrColors);

	//set the Color 0 to the value of the reference
	storedColors[0].r = MAX_OUTPUT_VALUE;
	storedColors[0].g = MAX_OUTPUT_VALUE;
	storedColors[0].b = MAX_OUTPUT_VALUE;
	
	//write the number of stored colors in the eeprom
	eeprom_write(eepromAddress + EEPROM_ADR_NR_COL, nrColors);

	//read in the reference 
	uart0_newLine(); uart0_newLine();
	uart0_puts("Referenz aufnehmen: Platzieren Sie den Robotoer ueber den Untergrund, danach druecken Sie die Leertaste");
	uart0_newLine(); 

	while(uart0_getc() != ' ');

	cs_setReference();

	//read in the color values
	uart0_newLine(); uart0_newLine();
	uart0_puts("Farben speichern: Platzieren Sie den Robotoer ueber die jeweilige Farbe, danach druecken Sie die Leertaste");
	uart0_newLine();

	for(uint8_t i = 1; i <= nrColors; i++) {
		uart0_newLine();
		uart0_puts("Farbe ");
		uart0_putCharAsDigits(i);
		uart0_putc(':');
		uart0_newLine();
		while(uart0_getc() != ' ');

		cs_setColor(i);

		uart0_puts("Farbe gespeichert!");
		uart0_newLine();
	}

 }

 void turnOnLEDs()
 {
	PORT_LEDS |= (1<<LEDS);
 }

 void turnOffLEDs()
 {
	 PORT_LEDS &= ~(1<<LEDS);
 }

 void toggleLEDs()
 {
 	PORT_LEDS ^= (1<<LEDS);
 }
 
void setOutFreqScaling(enum CS_FreqScale val)
{
	if (val <= 3)
	{
		PORT_S01 &= ~((1<<S0) | (1<<S1));
		PORT_S01 |=  (val<<S0);
	}
}

 void selColorOfSensor(enum CS_ColorSel val)
 {
	if (val <= 3)
	{
		PORT_S23 &= ~((1<<S2) | (1<<S3));
		PORT_S23 |=  (val<<S2);
	}
 }

 void selSensor(enum CS_SensorSel val)
 {
	if (val <= 3)
	{
		PORT_AB &= ~((1<<A) | (1<<B));
		PORT_AB |=  (val<<A);
	}
 }

 void startTimersForColorStoring()
 {
	TCCR4B = ((1<<CS40)); //select normal mode and prescaler to 0
	TCNT4 = 0;

	TCCR0B |= ((1<<CS00)|(1<<CS01)|(1<<CS02)); //set source to external on rising edge
	TCNT0 = 0;
	cntOVF0 = 0;
 }

 void startMeasurement(enum CS_SensorSel sensor, uint8_t color)
 {
	selSensor(sensor);

	if (color == 0)
		selColorOfSensor(RED);
	else if (color == 1)
		selColorOfSensor(GREEN);
	else if (color == 2)
		selColorOfSensor(BLUE);

	TCCR4B = ((1<<WGM42)|(1<<CS40)); //CTC mode with OCR4A as top value and prescaler to 0
	OCR4A = storedReference[sensor][color];

	TCCR0B |= ((1<<CS00)|(1<<CS01)|(1<<CS02)); //set source to external on rising edge
	TCNT0 = 0;
	cntOVF0 = 0;
 }

 void startTimersForReference()
 {
	TCCR4B = ((1<<WGM42)|(1<<CS40)); //CTC mode with OCR4A as top value and prescaler to 1
	OCR4A = 0xFFFF;
	TCNT4 = 0;

	TCCR0B |= ((1<<CS00)|(1<<CS01)|(1<<CS02)); //set source to external on rising edge
	TCNT0 = 0;
	cntOVF0 = 0;
 }

 void stopTimers()
 {
	TCCR4B = 0x00;
	TCCR0B = 0x00;
 }



void storeReferenceInEeprom()
{
	#ifdef _DEBUG_
		uart0_newLine();
	#endif
	for (uint8_t sensor = 0; sensor < 3; sensor++) {
		for (uint8_t color = 0; color < 3; color++) {
			eeprom_write(eepromAddress+EEPROM_ADR_REF+color*2+sensor*6, storedReference[sensor][color]);
			eeprom_write(eepromAddress+EEPROM_ADR_REF+color*2+sensor*6+1, storedReference[sensor][color]>>8);
			#ifdef _DEBUG_
 				uart0_putCharAsDigits16(storedReference[sensor][color]);
 				uart0_putc('\t');
 				uart0_putCharAsDigits(eepromAddress+EEPROM_ADR_REF+color*2+sensor*6);
 				uart0_putc('\t');
 				uart0_putCharAsDigits(storedReference[sensor][color]);
 				uart0_putc('\t');
 				uart0_putCharAsDigits(eepromAddress+EEPROM_ADR_REF+color*2+sensor*6+1);
 				uart0_putc('\t');
 				uart0_putCharAsDigits(storedReference[sensor][color]>>8);
 				uart0_newLine();
			#endif
		}
	}
}

//Ein Schreib-Zugriff auf EEPROM ca. 3.42ms gemessen

uint8_t readReferenceFromEeprom()
{
	#ifdef _DEBUG_
  		uart0_newLine();
  		uart0_newLine();
	#endif
	for (uint8_t sensor = 0; sensor < 3; sensor++) {
		for (uint8_t color = 0; color < 3; color++) {
			if ( (storedReference[sensor][color] = eeprom_read(eepromAddress+EEPROM_ADR_REF+color*2+sensor*6) ) == 0xFF)
				return 0;
			if ( ( storedReference[sensor][color] |= (eeprom_read(eepromAddress+EEPROM_ADR_REF+color*2+sensor*6+1)<<8) ) == 0xFF00)
				return 0;
			#ifdef _DEBUG_
  				uart0_putCharAsDigits16(storedReference[sensor][color]);
  				uart0_newLine();
			#endif
		}
	}
	return 1;
}
//Ein Lese-Zugriff auf EEPROM ca. 2.838us gemessen


void storeColorInEeprom(uint8_t colorNr)
{
	if (colorNr > 0 && colorNr <= nrStoredColors) {
		eeprom_write(eepromAddress+EEPROM_ADR_COLORS+(colorNr-1)*3, storedColors[colorNr].r);
		eeprom_write(eepromAddress+EEPROM_ADR_COLORS+(colorNr-1)*3+1, storedColors[colorNr].g);
		eeprom_write(eepromAddress+EEPROM_ADR_COLORS+(colorNr-1)*3+2, storedColors[colorNr].b);
		#ifdef _DEBUG_
			uart0_newLine();
			uart0_puts("Adr:");
			uart0_putCharAsDigits(eepromAddress+EEPROM_ADR_COLORS+(colorNr-1)*3);
		#endif
	}
}

void readColorFormEeprom(uint8_t colorNr)
{
	if (colorNr > 0 && colorNr <= nrStoredColors) {
		storedColors[colorNr].r = eeprom_read(eepromAddress+EEPROM_ADR_COLORS+(colorNr-1)*3);
		storedColors[colorNr].g = eeprom_read(eepromAddress+EEPROM_ADR_COLORS+(colorNr-1)*3+1);
		storedColors[colorNr].b = eeprom_read(eepromAddress+EEPROM_ADR_COLORS+(colorNr-1)*3+2);
		#ifdef _DEBUG_
			uart0_newLine();
			uart0_putCharAsDigits(storedColors[colorNr].r);
			uart0_putc(' ');
			uart0_putCharAsDigits(storedColors[colorNr].g);
			uart0_putc(' ');
			uart0_putCharAsDigits(storedColors[colorNr].b);
			uart0_putc(' ');
		#endif
	}
}

//take reference values, as many as the Constant NUM_REF_SAMPLES state, average them and store them in a globe array
//	and write them in the eeprom
uint8_t cs_setReference ()
{
	uint32_t	avg[3][3] = {	{0, 0, 0},
								{0, 0, 0},
								{0, 0, 0} };

	//take the samples for the ref
	for (uint8_t i = 0; i < NUM_REF_SAMPLES; i++) {
		for (uint8_t sensor = 0; sensor < 3; sensor++)
		{
			selSensor(sensor);
			for (uint8_t color = 0; color < 3; color++)
			{
				if (color == 0)
					selColorOfSensor(RED);
				else if (color == 1)
					selColorOfSensor(GREEN);
				else if (color == 2)
					selColorOfSensor(BLUE);
				
				//Start the timers/the measurement
				startTimersForReference();
				//wait until there were as many impulses as the const state
				while(TCNT0 < MAX_OUTPUT_VALUE);
				//stop the timers
				stopTimers();
				//add the value of the Timer4 to the average value
				avg[sensor][color] += TCNT4;
			}
		}
	}

	//calculate the average
	for (uint8_t sensor = 0; sensor < 3; sensor++)
	{
		for (uint8_t color = 0; color < 3; color++)
		{
			//store the average value in the global array
			storedReference[sensor][color] = avg[sensor][color]/NUM_REF_SAMPLES;
		}
	}

	//store the array in the eeprom
	storeReferenceInEeprom();
	//readReferenceFromEeprom();

	return 1;
}

//take color values of the specific colorNr, as many as the Constant NUM_COLOR_SAMPLES state, 
//	average them and store them in a globe array and write them in the eeprom
uint8_t cs_setColor (uint8_t colorNr)
{
	uint16_t	avg[3] = {0, 0, 0};

	//take the samples for the color to store
	for (uint8_t i = 0; i < NUM_COLOR_SAMPLES; i++) {
		for (uint8_t sensor = 0; sensor < 3; sensor++)
		{
			selSensor(sensor);
			for (uint8_t color = 0; color < 3; color++)
			{
				if (color == 0)
					selColorOfSensor(RED);
				else if (color == 1)
					selColorOfSensor(GREEN);
				else if (color == 2)
					selColorOfSensor(BLUE);

				
				//Start the timers/the measurement
				startTimersForColorStoring();
				//wait until the timer reach the value of the reference
				while (TCNT4 < storedReference[sensor][color]);
				//stop the timers
				stopTimers();

				if (cntOVF0 > 0)
					avg[color] += 0xFF; //if more pulses then the maximum happened -> take the value 0xFF
				else
					avg[color] += TCNT0; //else set the output to the measurement
			}
		}
	}

	//calculate the average and store it in the global array
	storedColors[colorNr].r = avg[0]/(NUM_COLOR_SAMPLES*3);
	storedColors[colorNr].g = avg[1]/(NUM_COLOR_SAMPLES*3);
	storedColors[colorNr].b = avg[2]/(NUM_COLOR_SAMPLES*3);

	#ifdef _DEBUG_
		uart0_newLine();
		uart0_putCharAsDigits(storedColors[colorNr].r);
		uart0_putc(' ');
		uart0_putCharAsDigits(storedColors[colorNr].g);
		uart0_putc(' ');
		uart0_putCharAsDigits(storedColors[colorNr].b);
		uart0_putc(' ');
	#endif

	//store the array in the eeprom
	storeColorInEeprom(colorNr);

	return 1;
}

//start the measurement and register the callback function
uint8_t cs_getColor(callback_func callbackFunc)
{
	if ( !measurementIsRunning ) {
		measurementIsRunning = 1;
		//store the callback function in a global variable
		callbackFunction = callbackFunc;

		//set the global temporary value to 0
		for (uint8_t i = 0; i < 3; i++) {
			tmpMeasureVal[i].r = 0;
			tmpMeasureVal[i].g = 0;
			tmpMeasureVal[i].b = 0;
		}

		//start the measurement
		startMeasurement(0,0);

		#ifdef _DEBUG_
			uart0_newLine(); uart0_newLine();
		#endif

		return 1;
	}
	else {
		return 0;
	}	
}

//calculate the difference of 2	RGB_Values and return them
uint8_t getColorDifference(struct RGB_Value color1, struct RGB_Value color2)
{
	return abs((int8_t)color1.r-color2.r) + abs((int8_t)color1.g-color2.g) + abs((int8_t)color1.b-color2.b);
}

struct RGB_Value zeroOutColor (struct RGB_Value color)
{
	struct RGB_Value colorOut;
	uint8_t minVal = color.r;

	if (color.g < minVal)
	minVal = color.g;
	else if (color.b < minVal)
	minVal = color.b;

	colorOut.r = color.r - minVal;
	colorOut.g = color.g - minVal;
	colorOut.b = color.b - minVal;

	return colorOut;
}

uint8_t getAvgOfColor(struct RGB_Value color)
{
	return (color.r+color.g+color.b+1) /3;
}

uint8_t getMedianOfColor(struct RGB_Value color)
{
	if (color.r > color.g) {
		if (color.g > color.b) {
			return color.g;
		} else if (color.r > color.b) {
			return color.b;
		} else {
			return color.r;
		}
	} 
	else {
		if (color.r > color.b) {
			return color.r;
		} else if (color.g > color.b) {
			return color.b;
		} else {
			return color.g;
		}
	}
}

uint8_t getColorDifferenceZeroed(struct RGB_Value color1, struct RGB_Value color2)
{
	uint8_t avgC1, avgC2;
	uint8_t difference = 0;

// 	avgC1 = getAvgOfColor(color1);
// 	avgC2 = getAvgOfColor(color2);

	avgC1 = getMedianOfColor(color1);
	avgC2 = getMedianOfColor(color2);

	difference = abs(((int8_t)color1.r-avgC1)-((int8_t)color2.r-avgC2));
	difference += abs(((int8_t)color1.g-avgC1)-((int8_t)color2.g-avgC2));
	difference += abs(((int8_t)color1.b-avgC1)-((int8_t)color2.b-avgC2));

	return difference;

	//return getColorDifference(zeroOutColor(color1), zeroOutColor(color2));
}



//determine the color, that is under each of the 3 sensors and return it
void determineAndReturnTheColor() {
	int8_t		colorOfMeasure[3];
	uint8_t		indexMinError;
	uint8_t		minError;

	for (uint8_t sensor = 0; sensor < 3; sensor++) {
		
		if (	tmpMeasureVal[sensor].r <= (MAX_OUTPUT_VALUE-THRESHOLD_DIFFERENCE) ||
				tmpMeasureVal[sensor].g <= (MAX_OUTPUT_VALUE-THRESHOLD_DIFFERENCE) ||
				tmpMeasureVal[sensor].b <= (MAX_OUTPUT_VALUE-THRESHOLD_DIFFERENCE)  	)
		{

			//initialize the minError with the difference of the first stored color
			minError = getColorDifferenceZeroed	(tmpMeasureVal[sensor], storedColors[1]);
			//set the index to the first stored color
			indexMinError = 1;

			//search through all stored colors
			for (uint8_t i = 2; i <= nrStoredColors; i++) {
				if (getColorDifferenceZeroed(tmpMeasureVal[sensor], storedColors[i]) < minError) {
					//if the difference is smaller than the smallest one before 
					//-> set the smalles to this one 
					minError = getColorDifferenceZeroed(tmpMeasureVal[sensor], storedColors[i]);
					indexMinError = i;
				}
				else if (getColorDifferenceZeroed(tmpMeasureVal[sensor], storedColors[i]) == minError) {
					indexMinError = -1;
				}

			}
		
			//store index of the smallest found difference in the array
			colorOfMeasure[sensor] = indexMinError;
		}
		else {
			colorOfMeasure[sensor] = 0;
		}
	}

	//return the array with the colors of the smalles difference
	if (callbackFunction != NULL)
		(*callbackFunction)(3, colorOfMeasure);
}

//increment the overflow counter if one happens
 ISR(TIMER0_OVF_vect)
 {
	 cntOVF0++;
 }

 //is called while the measurement is started
 ISR(TIMER4_COMPA_vect)
 {
 	static uint8_t	sensor = 0;
	static uint8_t	color = 0;

	stopTimers();

	//store the measured value in the global array
	if (color == 0)
		tmpMeasureVal[sensor].r = TCNT0;
	else if (color == 1)
		tmpMeasureVal[sensor].g = TCNT0;
	else if (color == 2)
		tmpMeasureVal[sensor].b = TCNT0;

	#ifdef _DEBUG_
		uart0_putCharAsDigits(TCNT0);
		uart0_putc(' ');
	#endif

	if (++color >= 3)
	{
		color = 0;

		#ifdef _DEBUG_
			uart0_putc('\t');
		#endif
		if (++sensor >= 3)
		{
			sensor = 0;
			
			//search thought the global array and return the found colors
			determineAndReturnTheColor();

			//stop measurement
			measurementIsRunning = 0;

			#ifdef _DEBUG_
				uart0_newLine();
			#endif
		}
		else {
			startMeasurement(sensor, color);
		}
	}
	else {
		startMeasurement(sensor, color);
	}
	
 }
