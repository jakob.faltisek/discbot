#include <avr/io.h>
#include <uart_basic.h>
#include <timebase.h>
#include "motioncontrol.h"
#include "cmdDispatcher.h"

//global variable that indicates if an command for the speed and direction was received
volatile uint8_t speedAndDirReceived;
//global variables for the received speed and direction
volatile int8_t speed;
volatile int8_t dir;

// Command:
// sets the speed and direction of the motor via the
// motion-control library
// |           |  CODE |  LEN  |   DATA0   |    DATA1   |
// |:----------|:-----:|:-----:|:---------:|:----------:|
// | COMMAND   | 0x10  |   2   |   speed	| direction  |
// | RESPONSE  | 0x10  |   0   |
#define CD_CMD_SET_SPEED_AND_DIR            0x10


// this function is called by the command dispatcher once a command
// has been received by the UART2, on which the BLE module is connected
void cd_executeCommand(uint8_t uartNo, uint8_t cmd, uint8_t len)
{
	switch (cmd)
	{
		case CD_CMD_SET_SPEED_AND_DIR:    
			// the speed and the direction shall be set
		
			//get the speed and direction from the command dispatcher
			speed = cd_getData(uartNo);
			dir = cd_getData(uartNo);

			//indicate that speed and direction was received
			speedAndDirReceived = 1;

			// send a response to the UART2
			cd_respond(uartNo, CD_CMD_SET_SPEED_AND_DIR, 0);  
		break;

		default:
			cd_respond(uartNo, CD_CMD_UNKNOWN, 0);  // in case an unknown command has been received
		break;  
	}

}

int main(void)
{
	//initialize the timebase
	tb_init(TB_10MS, 10); 

	//initialize the command dispatcher
	cd_init();
	cd_configUART(CD_UART2, 9600, 100, 200, 10); // initialize uart2 (BLE module) for the command dispatcher

	//initialize the motor driver via motion-control library
	mc_init(0);

	//set speed to 0 to stop the discbot at the beginning
	mc_setSpeed(0); 

	//initialize the global variables
	speed = 0;
	dir = 0;
	speedAndDirReceived = 0;

    while (1) 
    {
		if (speedAndDirReceived) //when speed and direction was received
		{
			speedAndDirReceived = 0;
			//set speed and direction on the motion-control library
			mc_setSpeed(speed);
			mc_setDirection(dir);
		}
	}
}


//____________________________________________________________
//To extend to required UART Lib for the command dispatcher
//  with functions to send a newLine and send a uint8_t value

void uart0_putCharAsDigits(uint8_t value);
void uart0_newLine();


void uart0_putCharAsDigits(uint8_t value)
{
	uint8_t h, z, e;
	
	h = value / 100;
	value = value % 100;
	z = value / 10;
	value = value % 10;
	e = value;
	
	if(h != 0)
	{
		uart0_putc(h + '0');
		uart0_putc(z + '0');
	}
	else
	{
		if(z != 0)
		{
			uart0_putc(z + '0');
		}
	}
	
	uart0_putc(e + '0');
}

void uart0_newLine()
{
	uart0_putc(13);
	uart0_putc(10);
}
//________________________________________

